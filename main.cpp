
#include "handles.hpp"

#include <gst/gst.h>

#include <iostream>

const int PORT = 6002;
const std::string IP = "192.168.202.122";

void build_play_pipeline(GstElement* pipelinePlay, GstElement* sourcePlay,
                         GstElement* parserPlay, GstElement* encoderPlay,
                         GstElement* rtppayPlay, GstElement* udpsinkPlay) {
  g_object_set(G_OBJECT(sourcePlay), "location", "bienvenidoTmp.wav", nullptr);

  gst_bin_add(GST_BIN(pipelinePlay), sourcePlay);
  gst_bin_add(GST_BIN(pipelinePlay), parserPlay);
  gst_bin_add(GST_BIN(pipelinePlay), encoderPlay);
  gst_bin_add(GST_BIN(pipelinePlay), rtppayPlay);
  gst_bin_add(GST_BIN(pipelinePlay), udpsinkPlay);

  gst_element_link(sourcePlay, parserPlay);
  gst_element_link(parserPlay, encoderPlay);
  gst_element_link(encoderPlay, rtppayPlay);
  gst_element_link(rtppayPlay, udpsinkPlay);
}

void build_dtmf_detector_pipeline(GstElement* pipelineDTMF,
                                  GstElement* sourceDTMF,
                                  GstElement* depayloaderDTMF,
                                  GstElement* rtpdecoderDTMF,
                                  GstElement* detectorDTMF,
                                  GstElement* fakesinkDTMF) {
  gst_bin_add(GST_BIN(pipelineDTMF), sourceDTMF);
  gst_bin_add(GST_BIN(pipelineDTMF), depayloaderDTMF);
  gst_bin_add(GST_BIN(pipelineDTMF), rtpdecoderDTMF);
  gst_bin_add(GST_BIN(pipelineDTMF), detectorDTMF);
  gst_bin_add(GST_BIN(pipelineDTMF), fakesinkDTMF);

  gst_element_link(sourceDTMF, depayloaderDTMF);
  gst_element_link(depayloaderDTMF, rtpdecoderDTMF);
  gst_element_link(rtpdecoderDTMF, detectorDTMF);
  gst_element_link(detectorDTMF, fakesinkDTMF);
}

int main() {
  std::cout << "main program starting...." << '\n';
  gst_init(nullptr, nullptr);

  std::cout << "Building play pipeline ......." << '\n';

  auto loopPlay = g_main_loop_new(nullptr, false);
  auto pipelinePlay = gst_pipeline_new("play_file");

  auto sourcePlay = gst_element_factory_make("filesrc", "file-source");
  auto parserPlay = gst_element_factory_make("wavparse", "parser");
  auto encoderPlay = gst_element_factory_make("mulawenc", "encoder");
  auto rtppayPlay = gst_element_factory_make("rtppcmupay", "rtppay");
  auto udpsinkPlay = gst_element_factory_make("udpsink", "udpsink");

  if (pipelinePlay == nullptr || sourcePlay == nullptr ||
      parserPlay == nullptr || encoderPlay == nullptr ||
      rtppayPlay == nullptr || udpsinkPlay == nullptr) {
    std::cerr << "One element could not be created. Exiting" << '\n';
  }

  build_play_pipeline(pipelinePlay, sourcePlay, parserPlay, encoderPlay,
                      rtppayPlay, udpsinkPlay);

  gst_element_set_state(pipelinePlay, GST_STATE_PAUSED);
  g_object_set(G_OBJECT(udpsinkPlay), "host", IP.c_str(), nullptr);
  g_object_set(G_OBJECT(udpsinkPlay), "port", PORT, nullptr);

  std::cout << "Building DTMF pipeline ......." << '\n';

  auto loopDTMF = g_main_loop_new(nullptr, false);
  auto pipelineDTMF = gst_pipeline_new("dtmf_detector");

  auto sourceDTMF = gst_element_factory_make("udpsrc", "udpsrc");
  auto depayloaderDTMF = gst_element_factory_make("rtppcmudepay", "depay");
  auto rtpdecoderDTMF = gst_element_factory_make("mulawdec", "decoder");
  auto detectorDTMF = gst_element_factory_make("dtmfdetect", "dtmfdetect");
  auto fakesinkDTMF = gst_element_factory_make("fakesink", "fakesink");

  if (!pipelineDTMF || !sourceDTMF | !depayloaderDTMF || !rtpdecoderDTMF ||
      !detectorDTMF || !fakesinkDTMF) {
    std::cerr << "One element could not be created. Exiting" << '\n';
  }

  build_dtmf_detector_pipeline(pipelineDTMF, sourceDTMF, depayloaderDTMF,
                               rtpdecoderDTMF, detectorDTMF, fakesinkDTMF);

  g_object_set(G_OBJECT(sourceDTMF), "host", IP.c_str(), nullptr);
  g_object_set(G_OBJECT(sourceDTMF), "port", PORT, nullptr);

  GstCaps* caps = gst_caps_from_string("application/x-rtp");
  g_object_set(G_OBJECT(sourceDTMF), "caps", caps, NULL);

  std::cout << "End of pipeline building......" << '\n';

  gst_element_set_state(pipelinePlay, GST_STATE_PLAYING);
  gst_element_set_state(pipelineDTMF, GST_STATE_PLAYING);

  // auto busPlay = gst_pipeline_get_bus(GST_PIPELINE(pipelinePlay));
  auto busDTMF = gst_pipeline_get_bus(GST_PIPELINE(pipelineDTMF));

  AppData data = {
      loopDTMF,
  };

  // gst_bus_add_watch(busPlay, bus_play_handle, &data);
  gst_bus_add_watch(busDTMF, bus_dtmf_detector_handle, &data);

  std::cout << "receiving messages from dtmf pipeline" << '\n';
  g_main_loop_run(loopDTMF);

  gst_element_set_state(pipelinePlay, GST_STATE_NULL);
  g_main_loop_unref(loopPlay);
  gst_object_unref(GST_OBJECT(pipelinePlay));
  //  gst_object_unref(busPlay);

  return 0;
}